import gnu.io.CommPortIdentifier;
import gnu.io.SerialPort;
public class SerialConnect {
    
    private SerialPort serialPort;
    private static final int BAUDRATE = 9600;
    
	public SerialPort connect(String COMPort) throws Exception {    
		CommPortIdentifier portIdentifier = CommPortIdentifier.getPortIdentifier(COMPort);    
	     
		if (portIdentifier.isCurrentlyOwned()) {
			System.out.println("you are connecting");  
			return null;
		}
		else {      
			serialPort = (SerialPort) portIdentifier.open("SerialConnect", 2000);   
			serialPort.setSerialPortParams(BAUDRATE, SerialPort.DATABITS_8, SerialPort.STOPBITS_1, SerialPort.PARITY_NONE);    
			return serialPort;
		}
	}
	 
}
