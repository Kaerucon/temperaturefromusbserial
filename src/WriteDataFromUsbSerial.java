import java.io.IOException;
import java.io.OutputStream;
public class WriteDataFromUsbSerial {
    OutputStream outputStream;    
    
    public WriteDataFromUsbSerial(OutputStream outputStream) {
        this.outputStream = outputStream;
    }    
        
    public void send(byte[] bytes) {    
        try {
        	outputStream.write(bytes);    
        	outputStream.flush();
        } catch (IOException e) {    
			System.out.println("WriteDataFromUsbSerial send() fail : "+e.getMessage());  
        }
    }
}
