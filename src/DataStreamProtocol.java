public interface DataStreamProtocol {  
    void onReceive(byte b);
    void onStreamClosed();   
}  