
import java.io.IOException;
import java.io.InputStream;

import gnu.io.CommPortIdentifier;  
import gnu.io.SerialPort;
public class ReadDataFromUsbSerial implements DataStreamProtocol {
	private byte[] readBuffer = new byte[1024];    
    private int tail = 0;    
    private String dataMessage;
    private Boolean readDataFinish = false;
    private InputStream inputStream;
    
    public ReadDataFromUsbSerial(InputStream inputStream) {    
        this.inputStream = inputStream;
    }    
    
    public int receive() {    
        try {
			return inputStream.read();
		} catch (IOException e) {
			System.out.println("ReadDataFromUsbSerial read() fail : "+e.getMessage());  
			return -1;
		}
    }
    
    public void onReceive(byte b) {
        if (b=='\n') {    
            onMessage();    
        } else {    
        	readBuffer[tail] = b;
            tail++;
        }    
    }       
         
    private void onMessage() {    
        if (tail!=0) {
        	setDataMessage(convertByteArrayToString(readBuffer));
            tail = 0; 
        	readDataFinish = true;
        }    
    }     
    
    private String convertByteArrayToString(byte[] byteArray) { 
        return byteArray.toString();    
    }  
    
    public void onStreamClosed() {    
        onMessage();
    } 
    public byte[] getMessageBytes() {
        return readBuffer;    
    }    
        
    public String getMessageString() { 
        return dataMessage;
    }  
    
    private void setDataMessage(String data) { 
    	dataMessage = data;
    } 


}
