

import java.io.*;
import gnu.io.SerialPort;


public class MainTest {
	private static SerialPort serialPort;
    private static final String COMPort = "COM5";
    private static ReadDataFromUsbSerial dataReceiver;
    private static WriteDataFromUsbSerial dataSender;
    
	public static void main(String[] args) throws Exception{

        byte init[] = {0x69, 0x00, 0x00, 0x00};
        byte shut[] = {0x6A, 0x00, 0x00, 0x00};
        byte measue[] = {0x11, 0x00, 0x00, 0x00};

		serialPort = new SerialConnect().connect(COMPort);
		System.out.println("serialPort COMPort : "+serialPort.getName());  
		
		dataReceiver = new ReadDataFromUsbSerial(serialPort.getInputStream());
		dataSender = new WriteDataFromUsbSerial(serialPort.getOutputStream());		
		
		write(init);
		Thread.sleep(1000);
		System.out.println("read init {0x69, 0x00, 0x00, 0x00} : "+ analyticsDataAndFormat(read()));
		Thread.sleep(5000);
		
		while(true){
			write(measue);
			Thread.sleep(2000);
			System.out.println("read measue {0x11, 0x00, 0x00, 0x00} : "+ analyticsDataAndFormat(read()));
		}
	}
	
	private static byte[] read(){
    	int b;  
		while((b = dataReceiver.receive()) != -1) {    
			dataReceiver.onReceive((byte) b);    
		}    
		dataReceiver.onStreamClosed();
		return dataReceiver.getMessageBytes();
	}	
	
	private static void write(byte[] byteArray) {
		dataSender.send(byteArray);
	}	
	
	private static String analyticsDataAndFormat(byte[] byteArray) {                   
    	if ((byteArray[2]&0x000000FF) == 0x11) {
    		return new StringBuilder().append(Integer.toHexString(byteArray[6] & 0x000000FF)).append(".").append(Integer.toHexString(byteArray[7] & 0x000000FF)).toString();
        }
            return null; 
    }	
	
}
